<?php

/**
 * @file
 */
namespace Drupal\biopama_caribbean_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;

/**
 * Creates a Caribbean Footer Block
 * @Block(
 * id = "block_caribbean_footer",
 * admin_label = @Translation("Caribbean Footer block"),
 * )
 */
class CaribbeanFooterBlock extends BlockBase implements BlockPluginInterface{

    /**
     * {@inheritdoc}
     */
    public function build() {
        return array (
			'#theme' => 'caribbean_footer',
        );
    }

}