<?php

/**
 * @file
 */
namespace Drupal\biopama_caribbean_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;

/**
 * Creates a Caribbean Header Block
 * @Block(
 * id = "block_caribbean_header",
 * admin_label = @Translation("Caribbean Header block"),
 * )
 */
class CaribbeanHeaderBlock extends BlockBase implements BlockPluginInterface{

    /**
     * {@inheritdoc}
     */
    public function build() {
        return array (
			'#theme' => 'caribbean_header',
        );
    }

}